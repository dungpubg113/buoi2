﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buoi2
{
    class Detail
    {
        public string Phone;
        public string Address;


        public static string XepLoai(double DiemTB)
        {
            if (DiemTB <= 10 && DiemTB >= 9)
            {
                return "Xuất sắc";
            }
            else if (DiemTB >= 7 && DiemTB < 9)
            {
                return "Giỏi";
            }
            else if (DiemTB >= 5 && DiemTB < 7)
            {
                return "Khá";
            }
            else if (DiemTB < 5 && DiemTB >= 0)
            {
                return "Yếu";
            }
            return null;
        }

        public void DemSoLuong(List<Student> student)
        {
            //2: Đếm số lượng loại sinh viên trong danh sách  VD dưới 5 - yếu, 5 - 7 khá, 7 - 9 giỏi, 9-10 xuất sắc 

            int Gioi, Kha, XuatSac, Yeu;

            XuatSac = student.Where(x => (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 >= 9 && (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 <= 10).Count();
            Console.WriteLine("Số lượng học sinh đạt xuất sắc: " + XuatSac);

            Gioi = student.Where(x => (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 >= 7 && (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 < 9).Count();
            Console.WriteLine("Số lượng học sinh đạt giỏi: " + Gioi);

            Kha = student.Where(x => (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 >= 5 && (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 < 7).Count();
            Console.WriteLine("Số lượng học sinh đạt khá: " + Kha);

            Yeu = student.Where(x => (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 > 0 && (x.Score1 + x.Score2 + x.Score3 + x.Score4) / 4 < 5).Count();
            Console.WriteLine("Số lượng học sinh đạt yếu: " + Yeu);
        }

        public void KiemTra(List<Student> students)
        {

            var KiemTra = from sv in students
                          select new
                          {
                              Ten = sv.Name,
                              DiemTB = (sv.Score1 + sv.Score2 + sv.Score3 + sv.Score4) / 4,
                              Loai = XepLoai((sv.Score1 + sv.Score2 + sv.Score3 + sv.Score4) / 4)
                          };
            foreach (var student in KiemTra)
            {
                Console.WriteLine($"Tên: {student.Ten}");
                Console.WriteLine($"Điểm trung bình: {student.DiemTB}");
                Console.WriteLine($"Loại: {student.Loai}\n");
            }

        }

        public void SinhVienYeu(List<Student> students)
        {
            //4: In ra các sinh viên yếu bị cảnh cáo
            var SVYeu = from sv in students
                        where (sv.Score1 + sv.Score2 + sv.Score3 + sv.Score4) / 4 < 5
                        select new
                        {
                            ID = sv.ID,
                            Ten = sv.Name,
                            DTB = (sv.Score1 + sv.Score2 + sv.Score3 + sv.Score4) / 4,
                            Class = sv.Class,
                            Score1 = sv.Score1,
                            Score2 = sv.Score2,
                            Score3 = sv.Score3,
                            Score4 = sv.Score4
                        };

            if (SVYeu.Count() > 0)
            {
                Console.WriteLine("Sinh viên yếu!! CẢNH BÁO");
                foreach (var student in SVYeu)
                {
                    Console.WriteLine($"ID: {student.ID}");
                    Console.WriteLine($"Tên: {student.Ten}");
                    Console.WriteLine($"Lớp: {student.Class}\n");
                    Console.WriteLine($"Score1: {student.Score1}\n");
                    Console.WriteLine($"Score2: {student.Score2}\n");
                    Console.WriteLine($"Score3: {student.Score3}\n");
                    Console.WriteLine($"Score4: {student.Score4}\n");
                    Console.WriteLine($"Điểm trung bình: {student.DTB}");
                }
            }
            else Console.WriteLine("Không có sinh viên yếu\n");

        }


       
        public void ThayDoi(List<Student> students)
        {
            //6: Thay đổi thông tin sinh viên.
            Console.Write("Nhập ID sinh viên muốn sửa: " + Console.ReadLine());
            var ID = Console.ReadLine();
            var sv = students.FirstOrDefault(s => s.ID.Equals(ID));
            if (sv != null)
            {
                Console.Write("Cap nhat ten: ");
                var name = Console.ReadLine();

                Console.Write("Cap nhat lop: ");
                var Class = Console.ReadLine();

                Console.Write("Cap nhat so dien thoai: ");
                var phone = Console.ReadLine();

                Console.Write("Cap nhat dia chi: ");
                var address = Console.ReadLine();


                sv.Name = name;
                sv.Class = Class;
                sv.Details.Phone = phone;
                sv.Details.Address = address;


                Console.WriteLine("Tên: " + name);
                Console.WriteLine("Lớp: " + Class);
                Console.WriteLine("SĐT: " + phone);
                Console.WriteLine("Địa chỉ: " + address);

            }
            else { Console.WriteLine("Không có ID này"); }
        }
        public void XoaSV(List<Student> students)
        {
            //7: Xoá sinh viên tại vị trí chỉ định: nếu không tìm đc sinh viên trả message không tìm đc sinh viên
            Console.Write("Nhập ID sinh viên muốn xóa: " + Console.ReadLine());
            var ID = Console.ReadLine();
            var sv = students.FirstOrDefault(s => s.ID.Equals(ID));
            if (sv != null)
            {
                students.Remove(sv);
                Console.WriteLine("Đã xóa sinh viên");
            }
            else Console.WriteLine("Không có sinh viên này");
        }

        public void TinKiem(List<Student> students)
        {
            //4: Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó: VD nhập Ngoc, thì in ra các sinh viên có tên là Ngoc
            Console.WriteLine("Nhập tên muốn tìm kiếm: " + Console.ReadLine());
            var name = Console.ReadLine();

            var TimKiem = (from s in students where s.Name.ToUpper().Contains(name.ToUpper()) select s).ToList();
            if (TimKiem.Count() > 0)
            {
                foreach (var student in TimKiem)
                {
                    Console.WriteLine("ID: " + student.ID);
                    Console.WriteLine("Tên: " + student.Name);
                    Console.WriteLine("Lớp: " + student.Class + " \n");
                }
            }
            else Console.WriteLine("Không có sinh viên này\n");

        }



    }
}
