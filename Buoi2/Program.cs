﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Buoi2
{
    class Program
    {
        //        Quản lý sinh viên:
        //-- Thông tin sinh viên:
        //---- Id
        //---- Name
        //---- Class
        //---- Score1
        //---- Score2
        //---- Score3
        //---- Score4
        //---- Details
        //------ Phone
        //------ Address

        //Dung Linq

        //1: Get all sinh viên
        //2: Đếm số lượng loại sinh viên trong danh sách
        //2: Xếp loại các sinh viên: VD dưới 5 - yếu, 5 - 7 khá, 7 - 9 giỏi, 9-10 xuất sắc 
        //3: In ra các sinh viên yếu bị cảnh cáo
        //4: Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó: VD nhập Ngoc, thì in ra các sinh viên có tên là Ngoc
        //5: Thay đổi thông tin sinh viên.
        //6: Xoá sinh viên tại vị trí chỉ định: nếu không tìm đc sinh viên trả message không tìm đc sinh viên
        static void Main(string[] args)
        {
            //làm ntn để lấy dc dữ liệu
            Console.OutputEncoding = Encoding.UTF8;
            // lay du lieu
            string path = @"Student.json";
            string data = System.IO.File.ReadAllText(path);
            //xu ly

            List<Student> items = JsonConvert.DeserializeObject<List<Student>>(data);
            char key;

            do
            {
                Console.WriteLine("\n----------------------------------------------------------------------");
                Console.WriteLine("        Chọn 1 trong các chức năng");
                Console.WriteLine("1. Chức năng in thông tin toàn bộ sinh viên");
                Console.WriteLine("2. Xếp loại các sinh viên và đếm số lượng sinh viên theo xếp loại");
                Console.WriteLine("3. In ra thông tin các sinh viên bị cảnh cáo");
                Console.WriteLine("4. Tìm kiếm thông tin sinh viên");
                Console.WriteLine("5. Thay đổi thông tin sinh viến");
                Console.WriteLine("6. Xóa thông tin sinh viên\n");
                Console.WriteLine("----------------------------------------------------------------------");
                Student student = new Student();
                Detail detail = new Detail();
                key = (char)Console.Read();
                switch (key)
                {
                    
                    case '1':
                        Console.Write("Chức năng in toàn bộ thông tin sinh viên\n");
                        student.InSinhVien(items);
                        break;
                    case '2':
                        Console.Write("Chức năng xếp loại các sinh viên và đếm số lượng sinh viên theo xếp loại\n");
                        detail.KiemTra(items);
                        detail.DemSoLuong(items);
                        break;
                    case '3':
                        Console.Write("Chức năng in thông tin các sinh viên đang ở diện cảnh cáo\n");
                        detail.SinhVienYeu(items);
                        break;
                    case '4':
                        Console.Write("Chức năng tìm kiếm thông tin sinh viên\n");
                        detail.TinKiem(items);
                        break;
                    case '5':
                        Console.Write("Chức năng thay đổi thông tin sinh viên\n");
                        detail.ThayDoi(items);
                        break;
                    case '6':
                        Console.Write("Chức năng xóa thông tin sinh viên\n");
                        detail.XoaSV(items);
                        break;
                    default:
                        Console.WriteLine("Phím bạn ấn không phải là số!\n");
                        break;
                }
                Console.ReadKey();
            } while (key != 1);

           
        }      
    }
}
