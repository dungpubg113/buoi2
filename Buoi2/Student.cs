﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Buoi2
{
    class Student
    {
        public string ID;
        public string Name;
        public string Class;
        public int Score1;
        public int Score2;
        public int Score3;
        public int Score4;
        public Detail Details;


        public void InSinhVien(List<Student> student)
        {
            var inSinhVien = from sv in student select new{
                sv.ID,
                sv.Name,
                sv.Class,
                sv.Score1,
                sv.Score2,
                sv.Score3,
                sv.Score4,
                sv.Details
            };
            foreach (var sv in inSinhVien)
            {
                Console.WriteLine($"ID: {sv.ID}");
                Console.WriteLine($"Tên: {sv.Name}");
                Console.WriteLine($"Lớp: {sv.Class}");
                Console.WriteLine($"Điểm 1: {sv.Score1}");
                Console.WriteLine($"Điểm 2: {sv.Score2}");
                Console.WriteLine($"Điểm 3: {sv.Score3}");
                Console.WriteLine($"Điểm 4: {sv.Score4}");
                Console.WriteLine($"Địa chỉ: {sv.Details.Address}");
                Console.WriteLine($"SĐT: {sv.Details.Phone}\n");
            }


        }
       

    }
}
